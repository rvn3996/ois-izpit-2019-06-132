/* global L, $ */

// seznam z markerji na mapi
var markerji = [];

var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {

  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 9,
    maxZoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Akcija ob kliku na gumb za prikaz zgodovine
  $("#prikaziZgodovino").click(function() {
    window.location.href = "/zgodovina";
  });

  function obKlikuNaMapo(e) {
    // Izbriši prejšnje oznake
    for (var i = 0; i < markerji.length; i++) {
      mapa.removeLayer(markerji[i]);
    }

    var prviKraj;

    var koordinate = e.latlng;

    // Dodaj trenutno oznako
    dodajMarker(koordinate.lat, koordinate.lng,
      "<b>Izbrana lokacija</b><br>(" + koordinate.lat + ", " + koordinate.lng + ")");

    var apiString = "https://api.lavbic.net/kraji/lokacija?lat=4" + koordinate.lat + "&lng=" + koordinate.lng;

    
    
     var krajiHtml = "<h3>Bližnji kraji</h3>";
    
     $.getJSON(apiString, function(data){
       
       $.each(data, function(index, value){
         if(index < 5){
           if(index == 0){
             prviKraj = value;
             
             
             
            $.get("/vreme/" + prviKraj.postnaStevilka, function(data, status){
              $("#vremeOkolica").html("<h3>Vreme v okolici kraja<br>" + prviKraj.kraj + "<br></h3><br>" + '<img class="slika" src="/slike/' + data.ikona + '"></img> ' + data.opis + " " + data.temperatura);
            });
           
            $.get('/zabelezi-zadnji-kraj/' + JSON.stringify({
              postnaStevilka: prviKraj.postnaStevilka,
              kraj: prviKraj.kraj,
              vreme: prviKraj.opis,
              temperatura: data.temperatura
            }), function(t) {
              $("#prikaziZgodovino").html("Prikaži zgodovino " + t.steviloKrajev + " krajev");
            });
             
           }
           
           
           krajiHtml += "<br><b>" + value.postnaStevilka + "</b> " + value.kraj;
           
           
         }
      });
      
      $("#bliznjiKraji").html(krajiHtml);
       //data.forEach(item, index)
    });
    
  }

  mapa.on('click', obKlikuNaMapo);

  // Na začetku postavi lokacijo na FRI
  var FRIpoint = new L.LatLng(FRI_LAT, FRI_LNG);
  mapa.fireEvent('click', {
    latlng: FRIpoint,
    layerPoint: mapa.latLngToLayerPoint(FRIpoint),
    containerPoint: mapa.latLngToContainerPoint(FRIpoint)
  });
});


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 */
function dodajMarker(lat, lng, opis) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  var marker = L.marker([lat, lng], {icon: ikona});
  marker.bindPopup("<div>" + opis + "</div>").openPopup();
  marker.addTo(mapa);
  markerji.push(marker);
}
